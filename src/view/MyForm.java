package view;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

abstract class MyForm extends JFrame{
    Integer WIDTH = 600, HEIGHT = 450;
    Integer atom = 30;
    List<JTextArea> textAreas = new ArrayList<>();
    List<JButton> buttons = new ArrayList<>();
    List<JLabel> labels = new ArrayList<>();
    MyForm(String title){
        super(title);
        //-------Создание формы
        setLayout(null);
        setSize(WIDTH, HEIGHT);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setResizable(false);
        setLocationRelativeTo(null);
        setVisible(true);
        //---------------------
    }

    void addListsOnForm(){
        for(JTextArea i : textAreas){
            add(i);
        }
        for(JButton i : buttons){
            add(i);
        }
        for(JLabel i : labels){
            add(i);
        }
    }
}
