package view;

import javax.swing.*;
import java.awt.*;

public class InputForm extends MyForm{
    public InputForm(){
        super("Дуффинг");
        createBaseElementsOnForm();
        addListsOnForm();
    }

    private void createBaseElementsOnForm(){
        JLabel logo = createElement(new JLabel("Logo"), 0, 0, WIDTH - 20, 3 * atom);     labels.add(logo);
        JLabel alpha = createElement(new JLabel("α="),  2 * atom, 4 * atom, atom, atom); labels.add(alpha);
        JLabel beta = createElement(new JLabel("β="),   8 * atom, 4 * atom, atom, atom); labels.add(beta);
        JLabel gamma = createElement(new JLabel("γ="), 14 * atom, 4 * atom, atom, atom); labels.add(gamma);
        JLabel F = createElement(new JLabel("F(t)="),   2 * atom, 6 * atom, atom, atom); labels.add(F);

    }

    private <T> T createElement(T element, int x, int y, int width, int height){
        ((Component)element).setBounds(x, y, width, height);
        return element;
    }


}
