package view;

public enum methodName {
    puz, puzMod, m2, m3, m3Mod, m4, m5, m6, m7, m8, m9;

    String getStringName(methodName methodName) {
        String stringNames[] = {
                "Метод Пузынина",
                "Метод Пузынина(Мод)",
                "Метод 2",
                "Метод 3",
                "Метод 3(Мод)",
                "Метод 4",
                "Метод 5",
                "Метод 6",
                "Метод 7",
                "Метод 8",
                "Метод 9"
        };
        return stringNames[methodName.ordinal()];
    }
}
