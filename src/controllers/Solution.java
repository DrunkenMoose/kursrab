package controllers;

public class Solution {
    double myAlpha = 1e-5;
    private double alpha, beta, gamma;
    private double a, b;
    private double A, B;
    private int n;
    private double tao = 0.0;
    private double[] t = null;

    public Solution(double alpha, double beta, double gamma, double A, double B) {
        this.alpha = alpha;
        this.beta = beta;
        this.gamma = gamma;
        this.A = A;
        this.B = B;
    }

    public void start(double a, double b, int n) {
        this.a = a;
        this.b = b;
        this.n = n;
        this.tao = (this.b - this.a) / (this.n - 1);
        this.t = generateT();

        Matrix x = new Matrix(generateX0(a, b, n));
        //x.printMatrix(); System.out.println();

        solve(x, 0.001);
    }

    private void solve(Matrix x, double myBeta){
        int countIter = 0;
        fx(x).printMatrix();
        while (true) {
            if(fx(x).norma() < 1e-5 && countIter < 1000){
                System.out.println(x);
                break;
            }
            if(countIter > 1000){
                System.out.println("Не сошёлся");
                System.out.println(fx(x).norma());
                break;
            }
            Matrix deltaX = fx_(x).multiply(fx(x)).multiply(-myBeta);
            Matrix newX = x.plus(deltaX);
            myBeta = Math.min(1, myBeta * fx(x).norma() / fx(newX).norma());
            x = new Matrix(newX);
            countIter++;
        }


    }

    double betaMethod1(double beta, Matrix x, Matrix deltaX){
        double chisl = beta * fx(x).norma();
        double znam = fx(x.plus(deltaX)).norma();
        return Math.min(1, chisl / znam);
    }

    private double[][] generateX0(double a, double b, int n) {
        double xCells[][] = new double[n][1];
        for (int i = 0; i < n; i++) {
            //xCells[i][0] = a + Math.random() * (b - a);
            xCells[i][0] = 1 + t[i] * t[i] * t[i];
        }
        return xCells;
    }

    private double F(double t) {
        return 0;
    }

    private double[] generateT() {
        double[] t = new double[this.n];
        for (int i = 0; i < this.n; i++) {
            t[i] = a + i * tao;
        }
        return t;
    }

    private Matrix fx(Matrix x) {
        double[][] res = new double[n][1];
        res[0][0] = x.cells[0][0] - A;
        res[n - 1][0] = x.cells[n - 1][0] - B;
        for (int i = 1; i < n - 1; i++) {
            res[i][0] =  (  1.0 / tao / tao - alpha / (2 * tao)) * x.cells[i - 1][0];
            res[i][0] += ( -2.0 / tao / tao + beta + gamma * x.cells[i][0] * x.cells[i][0] ) * x.cells[i][0];
            res[i][0] += (( 1.0 / tao / tao + alpha / (2 * tao))) * x.cells[i + 1][0];
            res[i][0] -= F(t[i]);
        }
        return new Matrix(res);
    }

    //Proizvodnaya
    private Matrix fx_(Matrix x) {
        double[][] res = new double[n][n];
        res[0][0] = 1;
        res[n - 1][n - 1] = 1;
        for (int i = 1; i < n - 1; i++) {
            res[i][i - 1] =   1.0 / (tao * tao) - alpha / (2 * tao);
            res[i][i]     =   - 2 / (tao * tao) + beta + 3 * gamma * x.cells[i][0] * x.cells[i][0];
            res[i][i + 1] =   1.0 / (tao * tao) + alpha / (2 * tao);
        }
        return new Matrix(res);
    }
}
