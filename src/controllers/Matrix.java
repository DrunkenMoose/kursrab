package controllers;
public class Matrix {
    int n, m;
    double[][] cells;
    public Matrix(Matrix a){
        this.n = a.n;
        this.m = a.m;
        this.cells = a.cells;
    }
    public Matrix(double[][]a){
        this.n = a.length;
        this.m = a[0].length;
        this.cells = a;
    }
    public Matrix(int n, int m){
        this.n = n;
        this.m = m;
        cells = new double[n][m];
    }
    public Matrix multiply(Matrix B){
        Matrix result = new Matrix(n, B.m);
        double s = 0;
        for (int i = 0; i < result.n; ++i) {
            for (int j = 0; j < result.m; ++j) {
                s = 0;
                for (int k = 0; k < m; ++k) {
                    s += cells[i][k] * B.cells[k][j];
                }
                result.cells[i][j] = s;
            }
        }
        return result;
    }

    public Matrix multiply(double k){
        Matrix result = new Matrix(this.n, this.m);
        for (int i = 0; i < this.n ; i++)
            for (int j = 0; j < this.m ; j++)
                result.cells[i][0] = this.cells[i][j] * k;
        return result;
    }
    public Matrix minus(Matrix a){
        Matrix result = new Matrix(this.n, this.m);
        for (int i = 0; i < n ; i++)
            for (int j = 0; j < m ; j++)
                result.cells[i][j] = this.cells[i][j] - a.cells[i][j];
        return result;
    }
    public Matrix plus(Matrix a){
        Matrix result = new Matrix(this.n, this.m);
        for (int i = 0; i < n ; i++)
            for (int j = 0; j < m ; j++)
                result.cells[i][j] = this.cells[i][j] + a.cells[i][j];
        return result;
    }
    public double norma(){
        double result = 0;
        for (int i = 0; i < this.n; i++)
            for (int j = 0; j < this.m; j++)
                result += this.cells[i][j] * this.cells[i][j];
        return Math.pow(result, 1.0 / 2);
    }
    public Matrix fXn(Matrix xn){
//        Matrix result = new Matrix(this.n, this.n);
//        for (int i = 0; i < this.n; i++) {
//            for (int j = 0; j < this.n; j++) {
//                double k = this.cells[i][j] * xn.cells[j][0];
//                result.cells[i][j] = k;
//            }
//        }

        Matrix result = new Matrix(this.n, 1);
        for (int i = 0; i < this.n - 1; i++) {
            for (int j = 0; j < this.n ; j++) {
                double k = this.cells[i][j] * xn.cells[j][0];
                result.cells[i][0] += k;
            }
            result.cells[i][0] -= this.cells[i][n];
        }

        result.cells[n - 1][0] = 1; //����� ��� ��������� �� ���� 0

        for (int j = 0; j < n ; j++)
            result.cells[n - 1][0] *= xn.cells[j][0];

        result.cells[n - 1][0] -= 1;
        return result;
    }
    public void printMatrix(){
        for (int i = 0; i < this.n; i++) {
            for (int j = 0; j < this.m; j++)
                System.out.print(this.cells[i][j] + " ");
            System.out.println();
        }
    }
    public Matrix proizvod(Matrix xn){
        Matrix result = new Matrix(this.n, this.n);

        for (int i = 0; i < n - 1 ; i++)
            for (int j = 0; j < n; j++)
                result.cells[i][j] = this.cells[i][j];

        double pr = 1; //������������ ������ ���������
        for (int k = 0; k < n; k++)
            pr *= xn.cells[k][0];

        for (int j = 0; j < n ; j++)
            result.cells[n-1][j] = pr / xn.cells[j][0];

        return result;

    }
    public Matrix inverse(){
        int i, j, k;
        int size = this.n;
        Matrix E = new Matrix(size, size);
        Matrix temp = new Matrix(this);
        for (i = 0; i < size; i++) {
            E.cells[i][i] = 1;
        }
        for (k = 0; k < size; k++) {
            for (j = k + 1; j < size; j++) {
                temp.cells[k][j] /= temp.cells[k][k];
            }
            for (j = 0; j < size; j++) {
                E.cells[k][j] /= temp.cells[k][k];
            }
            temp.cells[k][k] = 1;

            if (k > 0) {
                for (i = 0; i < k; i++) {
                    for (j = 0; j < size; j++) {
                        E.cells[i][j] -= E.cells[k][j] * this.cells[i][k];
                    }
                    for (j = size - 1; j >= k; j--) {
                        temp.cells[i][j] -= temp.cells[k][j] * temp.cells[i][k];
                    }
                }
            }
            for (i = k + 1; i < size; i++) {
                for (j = 0; j < size; j++) {
                    E.cells[i][j] -= E.cells[k][j] * this.cells[i][k];
                }
                for (j = size - 1; j >= k; j--) {
                    temp.cells[i][j] -= temp.cells[k][j] * temp.cells[i][k];
                }
            }
        }
        return E;
    }
    static public Matrix E(int n){
        Matrix ans = new Matrix(n,n);
        for (int i = 0; i < n; i++) {
            ans.cells[i][i] = 1;
        }
        return ans;
    }
}
